/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ruleta;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;

/**
 *
 * @author guido
 */
public class Ruleta {
    private List<Integer> numeros;
    private final HashSet <Integer> lowAndThirdDozen; 
    private final HashSet <Integer> nonProfit;
    private List<Integer> favorables;
    private List<Integer> desfavorables;
    private double bankroll = 3000.00;
    private double apuesta = 5.00;
    private int winsInArow = 0;
    private int losesInArow = 0;
    
    public Ruleta(){
        numeros = new ArrayList<>();
        lowAndThirdDozen  = new HashSet <>(Arrays.asList(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,
                                                         16,17,18,25,26,27,28,29,30,31,32,33,34,35,36));
        nonProfit  = new HashSet <>(Arrays.asList(0,19, 20, 21, 22, 23, 24));
        favorables = new ArrayList<>();
        desfavorables = new ArrayList<>();
    }
    
    public void refillRulette(){
        for(int i = 0; i <= 36; i++){
            numeros.add(i);
        }
    }
    
    public void shuffleRulette(){
        Collections.shuffle(numeros);
    }
    
    public void spin(){
        Random rand = new SecureRandom();
        int index = rand.nextInt(37);
        Integer bola = numeros.get(index);
        if (lowAndThirdDozen.contains(bola)){ //si acertó hay que calcular la ganancia
            calculateWins(bola);
            favorables.add(bola);
        }
        else {
            calculateLoses(bola);
            desfavorables.add(bola);
        } 
    }
    
    public void calculateWins(int bola){
        winsInArow++;
        bankroll = bankroll + getApuesta() * 0.2;
        double gananciaTotal = getApuesta() + getApuesta()*0.2;
        System.out.println("Apuesta: $" + getApuesta() + " - Salió -> " + bola +  " - Ganancia: $" + gananciaTotal + " - Bankroll: $" + String.format("%.2f", getBankroll()));
        if (winsInArow >= 2) setApuesta(5.00);
        if (winsInArow >= 2) losesInArow = 0;
    }
    
    public void calculateLoses(int bola){
        winsInArow = 0;
        losesInArow++;
        bankroll = bankroll - getApuesta();
        System.out.println("Apuesta: $" + getApuesta() + " - Salió -> " + bola + " - Pérdida: $" + getApuesta() + " - Bankroll: $" + String.format("%.2f", getBankroll()));
        setApuesta(calcularApuestaBeforeLose());
    }
    
    public double calcularApuestaBeforeLose(){
        switch(losesInArow) {
            case 1:
                return 15;
            case 2:
                return 55;
            case 3:
                return 110;
            case 4:
                return 240;
            default:
                return 5.0;
        }
    }

    public double getBankroll() {
        return bankroll;
    }

    public void setBankroll(double bankroll) {
        this.bankroll = bankroll;
    }

    public double getApuesta() {
        return apuesta;
    }

    public void setApuesta(double apuesta) {
        this.apuesta = apuesta;
    }

    public List<Integer> getFavorables() {
        return favorables;
    }

    public void setFavorables(List<Integer> favorables) {
        this.favorables = favorables;
    }

    public List<Integer> getDesfavorables() {
        return desfavorables;
    }

    public void setDesfavorables(List<Integer> desfavorables) {
        this.desfavorables = desfavorables;
    }

    public List<Integer> getNumeros() {
        return numeros;
    }

    public void setNumeros(List<Integer> numeros) {
        this.numeros = numeros;
    }
    
}
