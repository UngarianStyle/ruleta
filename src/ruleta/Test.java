/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ruleta;

/**
 *
 * @author guido
 */
public class Test {

    public static void main(String[] args) {
        
        int cantSpines = 500;
        Ruleta ruleta = new Ruleta();
        
        ruleta.refillRulette();
        System.out.println(ruleta.getNumeros());
        ruleta.shuffleRulette();
        System.out.println(ruleta.getNumeros());
        for (int i=0; i < cantSpines; i++){
              ruleta.spin();
        }
        System.out.println(ruleta.getFavorables());
        System.out.println("Favorables: " + ruleta.getFavorables().size());
        System.out.println(ruleta.getDesfavorables());
        System.out.println("Desfavorables: " + ruleta.getDesfavorables().size());
    }
    
}
